package br.ucsal.bes20182.testequalidade.bancopovo;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;



public class LancamentoContaSteps {
	
	private ContaCorrente contaCorrente;

	@Given("abrir uma conta corrente")
	public void instanciarContaCorrente() {
		
		contaCorrente = new ContaCorrente();

	}
	@When("realizo um dep�sito de $valor")
	public void depositar(Double valor) {
		contaCorrente.depositar(valor);
	}
	@Then("o saldo da minha conta ser� de $saldo")
	public void verificaSaldoConta(String  situacaoEsperada) {
		Assert.assertEquals(situacaoEsperada, contaCorrente.consultarSaldo());
	}

}
